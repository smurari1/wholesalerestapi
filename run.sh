#!/bin/bash

if [ $# -ne 3 ]; then
	echo "Usage: run.sh <script_name> <token.csv> <report> "
	exit 1
fi

echo $1
echo $2
echo $3

jmeter -n -t $1 -l $2 -e -o $3
